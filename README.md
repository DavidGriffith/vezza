Sibling projects for the TRS-80s using the same common core: <br>
Model 1 - [M1ZVM](https://gitlab.com/sijnstra1/m1zvm/) - most front end came from this version <br>
Model 2 - contact me <br>
Model 3 - [M3ZVM](https://gitlab.com/sijnstra1/m3zvm/) <br>
Model 4/4p - [M4ZVM](https://gitlab.com/sijnstra1/m4zvm/) <br>
Embedded (48k/128k tape Spectrum) - [e-VEZZA](https://gitlab.com/sijnstra1/e-vezza) <br>

# Technical summary - Public Beta release:
~~~
VEZZA is a z80 based CP/M 3 (& 2.2) Infocom/Inform/Z-machine game executor and
    virtual machine. It buids in the TRS-80 M1ZVM port of M4ZVM which in turn
    originally started as ZXZVM. Much time has been spent optimising, adding
    features and debugging the code.
It can run v1-v8* inform format interactive fiction game files including
    Infocom and post-Infocom era story game files.
    *see features and limitations below
    Interactive Fiction games are available at numerous locations including:
    https://www.ifarchive.org/ just look for files with extension such as 
    .z1, .z2, .z3, .z4, .z5, .z6, .z7, .z8
Available builds (requires CP/M version 3 or compatible system):
  vezza-nb.com - 80x24 screen, vt52 terminal (e.g. Amstrad CPC)
  vezza-b.com  - 80x24 screen, vt52 + Banked CP/M 3
  vezza-51.com - 51x24 screen, vt52 terminal (e.g. Spectrum +3)
  vezza-90.com - 90x32 screen, vt52 terminal (e.g. PCW)
  vezza-h.com  - 80x25 screen, HGT + Banked CP.M 3 (e.g. CPU280)
  vezza-so.com - 80x24 screen, Soroc IQ 120 terminal (e.g. Apple Softcard CP/M3)
  vezza-h3.com - 80x24 screen, H19 + Banked CP/M 3 (e.g. Heathkit fully expanded)
  vezza-a3.com - 80x24 screen (trimmed), Banked, ADM-3a/TVI-912
   (tested on MicroBee; and also Commodore 128 under CP/M 3 in 80 column mode)
  vezza-MB.com - 80x24 screen (trimmed), Banked, ADM-3a + ANSI colour (Microbee)
Other builds (Large memory CP/M 2.2, no timed input):
  vezza-SC.com - 80x24 VT52 runninng SAMDos (SAMCoupe)
  vezza-A8.com - 80x24 RunCPM Adm3a - Atari 800 with FujiNet and DT80
  vezza-C2.com - 80x24 RunCPM VT100 (known stability issues)
  vezza-CC.com - 80x24 RunCPM VT100 with 256 ANSI colour codes
Smaller memory builds (CP/M 2.2, can't play BeyondZork, AMFV or Triity)
  vezza-h2.com - 80x24 screen, H19 terminal (e.g. Heathkit)
  vezza-MA.com - 80x24 screen (trimmed), ADM-3a/TVI-912 (e.g. MicroBee)
Slow builds due to BIOS limitations (extra register presevation, less cache, smaller memory build):
  Note: some optimising may be possible
  vezza-AV.com - CP/M 2.2 with VT100 codes plus 16 bit ANSI colour & high
                 RAM. Works on Agon Light CP/M 2.2
                 Note: Issues with very high I/O such as screen animations
  vezza-AX.com - CP/M 2.2 with VT100 codes plus 16 bit ANSI colour, high
                 RAM & FabGL Italic. Works on Agon Light CP/M 2.2
                 Note: Issues with very high I/O such as screen animations
  vezza-RW.com - CP/M 2.2 with VT100 codes plus 16 bit ANSI colour with low
                 RAM. Tested on RC 2014 SC-126 using TeraTerm
  vezza-K2.com - Bare CP/M 2.2 with Adm3a terminal codes and no highlighting
                 aimed at Kaypro II (80x24)
  vezza-k3.com - CP/M 3 with Adm3a terminal codes plus extra highlighting
                 for Kaypro with graphics capability
  vezza-X8.com - Bare CP/M 2.2 with Adm3a terminal codes and no highlighting
                 aimed at Xerox 820 (80x24) - no delete row so multi row
                 upper window will have cosmetic issues
  vezza-O1.com - 52x24 CP/M 2.2 with TVI-912 codes + extras (Osborne 1)
  vezza-BW.com - 79x25 CP/M 2.2 with TVI-912 codes + extras (Bondwell2/12)
Slow build as above with MSX-DOS code to support timed input
  vezza-M2.COM - 80x24 MSX-DOS - needs manual switch to 80 column "MODE 80"
  vezza-M1.COM - 40x24 MSX-DOS - needs manual switch to 40 column "MODE 40"

Written by Shawn Sijnstra (c) 2021-2 under GPLv2
Feedback, comments and bug reports welcome - email me <firstname>@<surname>.com
About me: home page of my various projects and other work http://sijnstra.com
~~~
# Inspiration:
~~~
The desire to buid this CP/M interpreter came from a number of sources. Firstly,
    I always wished I could play more recent Infocom games on my RehDesign CPU280
    and secondly, there seemed to be a broader desire for a fast, portable and
    compact CP/M infocom interpreter.
The design principles for this project are:
    First - speed of game execuction. Playability is key.
    Second - size of binary to allow the largest possible game on the one disc/disk.
    Third - portability to as many platforms as possible.
~~~
# Features:
~~~
Supports the core features needed including
* z1, z2, z3, z4, z5, z6, z7 and z8 games can execute
* Supports full screen width and multi line status (where terminal allows)
* Named game save and load
* Timed input on CP/M 3 and MSX-DOS (to support game such as BorderZone)
* Transcript (printing)
* Reverse text highlighting, including when selected using colour changing codes
* Underline and/or Bold where available on the terminal
* Colour where available (currently MicroBee & VT100, using VT100/ANSI colour codes)
* Accented chatacters are converted to a printable character
* Fast execution of games (even z3 games now execute faster than the original)
* z3 status line supports am/pm time including Cut Throats easter egg
* Non-banked CP/M supports up to 22K DynMem games (won't load Trinity or Beyond Zork
  for example), however, will load slightly faster.
* Banked CP/M version supports up to 37k DynMem (New: will even load Trinity)
* 2KB z-machine stack size (over 50 call levels deep)
* Uses standard BDOS calls
* Ctrl-A, Ctrl-F, Ctrl-G, Ctrl-H to edit via cursor left, cursor right, delete, backspace
  shift-left and shift-right also move the cursor left and right during line input
* command-line switch to optionally enable the infamous "Tandy bit". This will tweak the
  messages and the copyright block of some z3 games.
For release by release update technical details, check out the Changelog.
~~~
# Limitations and known bugs:
~~~
* Large and complex .z8 & .z5 games can execute slowly, especially if they use the inform7
  compiler or use the interpreter in a way that exceeds the compute power of 8 bits.
  The easiest rough guide is that post-infocom games are much more likely to run well if
  compiled using Inform6, especially the PunyInform library. Inform7 games are likely to
  be too complex.
* WARNING and Special note for *Inform 7* compiled games: The Inform 7 library is
  designed to use the stack a lot more than either Inform 6 or any of the genuine Infocom
  games. This means that even if an Inform 7 compiled game loads, and you are patient enough
  to wait for your next turn, as the game develops it may reach a point where the game runs
  out of stack and you can no longer progress that story. This would be disappointing to
  say the least.
* z6 support is very minimal and largely proof of concept only. The main games which are
  playable are Arthur and Shogun, noting the graphics are not supported. Unreleased games
  such as the German version of Zork I and Restaurant at the End of the Universe also work.
  Zork 0 is now somewhat playable. Journey is not.
  IMPORTANT NOTE: No graphics support also means that where the games have a graphic puzzle,
  that the puzzle will not be visibile. This includes one in Shogun, and multiple in Zork 0.
  It also means that the full graphics splash screens in Arthur will appear as blank screens
  with a cursor in the top left. Simply press <space> to continue over them.
* Highlight techniques and substitutions hardware dependent - i.e. using underline for Bold
  is in place and causes cosmetic issues. A bold-face space is still a space, underline draws
  an underline.
* To use full width, terminal must support turing off wrapping (i.e. discard at end of line)
* Requires a well behaved BIOS - this is causing issues with Kaypro and MSX
* To support status line properly, terminal must support delete line or windowing
* Only one font (font 2 and font 3 not supported)
* Timer limited to 1 second intervals
* Upper window/status cosmetic issues are expected as most terminal emulations do not have the
  features needed to protect upper rows. 100% coverage of upper window works properly.
* Sound, graphics not supported (although tempted to look at HR graphics support for splash)
* No support for Undo (note that Vezza correctly reports this)
* Terminal hardware compatibility will limit feature availability on some platforms
* Transcript is to printer only, assumes same width as screen, and doesn't convert accented characters
* Upper window does not auto-expand, however, it does allow the text to be displayed regardless
* Testing is minimal
~~~
# Screenshots:
~~~
Djinn on the Rocks (z5 PunyInform game) running on Arnold in CPC6128 mode:
~~~
![Djinn on the Rocks screenshot](CPC_DOTR.PNG "DOTR (z5 game) running on Arnold")
~~~
Nord and Bert couldn't make head nor tail of it (z4 game) running on Speccy Spectrum emulator:
~~~
![Nord_and_Bert screenshot](Speccy_NordandBert.PNG "Nord and Bert (z4 game) running on Speccy")
~~~
Zork: Bureaucracy (z4 game) running on REHDesign CPU280 with HGT terminal:
~~~
<img src="Vezza_Bureau_CPU280.jpg" alt="CPU280 Bureaucracy screenshot" width=600>

~~~
Zork: The Undiscovered Underground (z5 game) running on AppleWin emulator under CP/M 3, using EMULA.COM for Soroc control codes and with additional hires card in slot 3:
~~~
![ZTUU screenshot](Apple2_ztuu.PNG "ZTUU (z5 game) running on AppleWin")

# Roadmap:
~~~
In no particular order, there are some additional plans
* VT100 support
* ANSI support
* Support mapping of other keys such as Fn keys and arrows
  this will be challenging as all keyboards are different
* ~~Banked BIOS support allowing larger DynMem games~~
    Done. Now need to combine into single code base if possible
* Publishing the source code when it's ready
* CP/M 2.2 support
* Other secret plans
Things unlikely to happen:
* Specific hardware use for additional memory
~~~
# Credits and thanks:
~~~
Stefan Vogt for his encouragement, support and use of my code on Hibernated 
  (https://8bitgames.itch.io/hibernated1) and other projects
John Elliot who created and shared the original ZXZVM code https://www.seasip.info/ZX/zxzvmport.html
And Garry Lancaster who made it even more portable https://gitlab.com/garrylancaster/zxzvm
George Phillips et al who created zmac http://48k.ca/zmac.html 
  and the all purpose TRS-80 emulator trs80gp http://48k.ca/trs80gp.html
  as well as providing support on hyperbnk and the detecting Model 4/4p tip
Fredrik Ramsberg for sharing his knowledge and experience from his own Ozmoo interpreter
  build experience https://github.com/johanberntsson/ozmoo
  as well as convincing me to work hard on improving the performance of the interpreter.
Ian Mavrick, Arno Puder, Jean-François DEL NERO for their hardware contrubtions, making
  these old machines easier to use in a modern world - between the 4Cellerator, TRS-IO
  and HxC2001 I have a faster CPU and more memory, access to transfer files over wifi,
  and a floppy drive emulator to give me reliability.
A shout out to people who have provided me feedback to keep making this better - especially
  Jonathan Hoof who has provided an amazing amount of feedback and testing, helping to
  make this as fast and reliable as it can be!

The Retrocomputing community for making documentation available including
  BDOS codes http://members.iinet.net.au/~daveb/cpm/bdos.html
SYDTRUG and its members over the years for getting me into the TRS-80 in the first place
  and supporting my curiosity. It's also the place where I first got the inspiration to
  want to do this so many years ago.
And of course Infocom and really so many others who made this all possible....
~~~

